import commands

def get_username(account_name):
    cmd = "pass show %s | grep 'login\:\ ' | sed 's/login\:\ //g'" % account_name
    (status, output) = commands.getstatusoutput(cmd)
    return output

def get_password(account_name):
    cmd = "pass show %s | head -n 1" % account_name
    (status, output) = commands.getstatusoutput(cmd)
    return output
