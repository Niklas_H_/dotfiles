function fish_prompt
    set -l last_status $status
    
    #set_color purple
    #date "+%d.%m.%y %H:%M:%S"
    #set_color normal
    
    # User
    set_color $fish_color_user
    echo -n (whoami)
    set_color normal
    
    echo -n '@'
    
    # Host
    set_color $fish_color_host
    echo -n (prompt_hostname)
    set_color normal
    
    echo -n ' '
    
    # PWD
    set_color $fish_color_cwd
    echo -n (prompt_pwd)
    set_color normal
    
    # Git
    if not set -q __fish_git_prompt_show_informative_status
        set -g __fish_git_prompt_show_informative_status 1
    end
    if not set -q __fish_git_prompt_color_branch
        set -g __fish_git_prompt_color_branch brmagenta
    end
    if not set -q __fish_git_prompt_showupstream
        set -g __fish_git_prompt_showupstream "informative"
    end
    if not set -q __fish_git_prompt_showdirtystate
        set -g __fish_git_prompt_showdirtystate "yes"
    end
    if not set -q __fish_git_prompt_color_stagedstate
        set -g __fish_git_prompt_color_stagedstate yellow
    end
    if not set -q __fish_git_prompt_color_invalidstate
        set -g __fish_git_prompt_color_invalidstate red
    end
    if not set -q __fish_git_prompt_color_cleanstate
        set -g __fish_git_prompt_color_cleanstate brgreen
    end
    
    set_color normal 
    printf '%s%s' (set_color normal) (__fish_git_prompt)
    set_color normal

    if not test $last_status -eq 0
        set_color $fish_color_error
    end

    #__fish_git_add_files
    #echo $CMD_DURATION
    
    echo
    
    echo -n '➤ '
    set_color normal
end
