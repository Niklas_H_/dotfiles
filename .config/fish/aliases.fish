alias discord   'Discord'

alias bb        './bb.sh'
alias bbe       './bb.sh edit'
alias bbp       './bb.sh post'
alias bbr       './bb.sh rebuild'

alias gc        'git yolo -a'
alias gp        'git push'
alias gcp       'git yolo -a; git push'

alias la        'ls -al'
alias pinentry  'pinentry-gtk-2'

alias sudo      'sudo -E'
alias svim      'sudo -E nvim'

alias vi        'nvim'
alias nano      'nvim'
alias vim       'nvim'

